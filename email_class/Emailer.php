<?php

//Emailer Class definition

class Emailer {

	private $sendTo="";
	private $sentFrom="";
	private $emailMsg="";
	private $emailSubject="";
	
	public function __construct()
	{
			
	}
	

	public function setSendTo($inSendTo)
	{
		$this->sendTo = $inSendTo;
	}


	public function setSentFrom($inSentFrom)
	{
		$this->sentFrom = $inSentFrom;	
	}
	
	
	public function setEmailSubject($inEmailSubject)
	{
		$this->emailSubject = $inEmailSubject;	
	}


	public function setEmailMsg($inEmailMsg)
	{
		$inEmailMsg = htmlentities($inEmailMsg);			//handles special characters
		$inEmailMsg = wordwrap($inEmailMsg,70,"\n");	//sentence length 70 characters and breaks
		$this->emailMsg = $inEmailMsg;				//store modified message in object property
	}

	
	public function getSentTo()
	{
		return $this->sentTo;	
	}
	
	
	public function getSentFrom()
	{
		return $this->sentFrom;	
	}
	
	
	public function getEmailSubject()
	{
		return $this->emailSubject;
	}


	public function getEmailMsg()
	{
		return $this->emailMsg;	
	}
	
	
	public function sendEmail()
	{
		$headers = "From: $this->sentFrom" . "\r\n";
		//echo "<h2>Headers $headers</h2>";
		//echo "<h2>Message $this->emailMsg</h2>";		
		return mail($this->sendTo,$this->emailSubject,$this->emailMsg,$headers); 
	}
	
	
}

?>
