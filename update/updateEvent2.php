<?php

	$edit_id = $_GET['event_id'];
	$eventName = "";
	$eventDesc= "";
	$eventPresenter = "";
	$eventDate = "";
	$eventTime = "";

  //error messages
	$eventNameErrMsg = "";
  $eventDescErrMsg = "";
	$eventPresenterErrMsg = "";
	$eventDateErrMsg = "";
	$eventTimeErrMsg = "";

	$validForm = False;

		if(isset($_POST["done"]))
	{
    $eventName = $_POST['eventName'];
  	$eventDesc= $_POST['eventDescription'];
  	$eventPresenter = $_POST['eventPresenter'];
  	$eventDate = $_POST['eventDate'];
  	$eventTime = $_POST['eventTime'];
		$Clicked = true;


		function validateName($inName){

			global $validForm, $eventNameErrMsg;

			if(preg_match('/^[a-zA-Z0-9,.!? ]*$/', $inName)){

			}
			else{

				$validForm = False;
				$eventNameErrMsg = "No numbers or special characters allowed";
			}
		}//end validateName()

		function validateDescription($inDescription){

			global $validForm, $eventDescErrMsg;

			if(preg_match('/^[a-zA-Z0-9,.!? ]*$/', $inDescription)){

			}
			else{

				$validForm = False;
				$eventDescErrMsg = "No numbers or special characters allowed";
			}
		}//end validateDescription()

		function validatePresenter($inPresenter){

			global $validForm, $eventPresenterErrMsg;

			if(preg_match('/^[a-zA-Z0-9,.!? ]*$/', $inPresenter)){

			}
			else{

				$validForm = False;
				$eventPresenterErrMsg = "No numbers or special characters allowed";
			}
		}//end validatePresenter()

		function validateDate($inDate){

			global $validForm, $eventDateErrMsg;



			if( strtotime(date("Y/m/d")) < strtotime($inDate) ){

			}
			else{

				$validForm = False;
				$eventDateErrMsg = "Please select a future date";
			}
		}//end validateDate()

		function validateTime($inTime){

			global $validForm, $eventTimeErrMsg;


			$time = strtotime($inTime);

			if(date('H',$time) < 22 && date('H',$time) > 6){

			}
			else{

				$validForm = False;
				$eventTimeErrMsg = "No time after 10 PM or before 6 AM";
			}
		}//end validateTime()

		$validForm = true;

		validateName($inName);
		validateDescription($inDescription);
		validatePresenter($inPresenter);
		validateDate($inDate);
		validateTime($inTime);


		if($validForm == true){

			include("connectPDO.php");


			$stmt = $conn->prepare("UPDATE wdv341_event SET event_name = :event_name, event_description = :event_description, event_presenter = :event_presenter, event_date = :event_date, event_time = :event_time WHERE event_id = '$edit_id'");

			$stmt->bindParam(':event_name', $event_name);
			$stmt->bindParam(':event_description', $event_description);
			$stmt->bindParam(':event_presenter', $event_presenter);
			$stmt->bindParam(':event_date', $event_date);
			$stmt->bindParam(':event_time', $event_time);

			$event_name = $inName;
			$event_description= $inDescription;
			$event_presenter= $inPresenter;
			$event_date= $inDate;
			$event_time= $inTime;

      echo 'Event is entered';

		}
	}
?>



<!DOCTYPE html>
<html>
<head>
</head>
<body>
</h2>

	<?php

	include 'select.php';			//connects to the database


?>


      <form id="form1" name="form1" method="post" action="updateEvent.php">
         <input type= "text" name= "eventName" value= "<?php echo $eventName?>" >
         <input type= "text" name= "eventDescription" placeholder="event description">
         <input type= "text" name= "eventPresenter" value= "<?php echo $eventPresenter; ?>">
         <input type= "text" name= "eventDate" placeholder="event date">
         <input type= "text" name= "eventTime" placeholder="event time">
         <input type= "submit" name= "done" value="insert">
      </form>
  </body>
</html>
