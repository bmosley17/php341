<?php
//variable data
$eventName= "";
$event_description= "";
$event_presenter = "";
$event_date = "";
$event_time = "";

//err msg

$eventNameErr= "";
$event_descriptionErr= "";
$event_presenterErr = "";
$event_dateErr = "";
$event_timeErr = "";

$validForm = true;

 if (isset($_POST['done']))
	 {

		 $eventName = $_POST['eventName'];

		 function validateEventName($eventName){
		 global $validForm, $eventNameErr;

		 $eventNameErr = "";

		 if(preg_match('/^[a-zA-Z0-9,.!? ]*$/', $eventName)){

		 }
		 else{

			 $validForm = False;
			 $eventNameErr = "No numbers or special characters allowed";
		 }
	 }// validateEventName()

	 $validForm = true;

	 validateEventName($eventName);

try{
	 include 'connectPDO.php';


 // prepare sql and bind parameters
 $stmt = $conn->prepare("INSERT INTO wdv341_event (event_name, event_description, event_presenter, event_date, event_time)
 VALUES (:event_name, :event_description, :event_presenter, :event_date, :event_time )");
 $stmt->bindParam(':event_name', $eventName);
 $stmt->bindParam(':event_description', $event_description);
 $stmt->bindParam(':event_presenter', $event_presenter);
 $stmt->bindParam(':event_date', $event_date);
 $stmt->bindParam(':event_time', $event_time);



 $stmt->execute();
 echo "Event Entered";

	}

 catch(PDOException $e)
	 {
	 echo "Connection failed: " . $e->getMessage();
	 }
}//ends if submit

?>
<form method="post">
 <input type= "text" name= "eventName" placeholder="event name">
 <input type= "text" name= "event_description" placeholder="event description">
 <input type= "text" name= "event_presenter" placeholder="event presenter">
 <input type= "text" name= "event_date" placeholder="event date">
 <input type= "text" name= "event_time" placeholder="event time">
 <input type= "submit" name= "done" value="insert">
</form>
