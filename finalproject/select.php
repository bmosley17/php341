<?php

$servername = "localhost";
$username = "root";
$password = "";
$database= "final";


$conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
// set the PDO error mode to exception
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
echo "Connected successfully";
$stmt = $conn->prepare("SELECT dvd_id, dvd_name, release_year, dvd_type FROM current_dvds");
$stmt->execute();
?>
<!DOCTYPE html>
<html>
<head>
  <style>

    body{
      background-image: url(wall_e.jpg);
    }
    body a{
      color: green;
      margin-left: 12px;
    }
    body a:hover{
        color: black;
        background-color: white;
    }
    table{
      background-color: grey;
      color: lightblue;
      text-align: center;
    }
    table td:hover{
        background-color: purple;
        color:gold;
    }

    table a:hover{
        background-color: gold;
        color:purple;
    }

    table a{
        background-color: pink;
        color:grey;
    }
    h1 {
      text-align: center;
      color: #F20DDB; 
    }


    </style>
</head>
<body>
  <h1> My DVD Collection </h1>
<?php
    echo "<table border= 2 align='center'>";
while  ($row=$stmt->fetch(PDO::FETCH_ASSOC)){

    echo "<tr>";
      echo "<td>" . $row['dvd_id'] . "</td>";
      echo "<td>" . $row['dvd_name'] . "</td>";
      echo "<td>" . $row['release_year'] . "</td>";
      echo "<td>" . $row['dvd_type'] . "</td>";
      echo "<td><a href='updateEvent.php?edit_id=" . $row['dvd_id'] . "'>Update</a></td>";
		  echo "<td><a href='deleteEvent.php?del_id=" . $row['dvd_id'] . "'>Delete</a></td>";

    echo "</tr>";
    }

    echo "</table>";



?>
    <a href= "login_success.php">Return to Admin Options</a>
</body>
</html>
