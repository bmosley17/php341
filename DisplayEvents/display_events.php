<?php

$servername = "localhost";
$username = "root";
$password = "";
$database= "skillstest";


$conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
// set the PDO error mode to exception
$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
echo "Connected successfully";
$stmt = $conn->prepare("SELECT event_id, event_name, event_description, event_presenter, event_day, event_time FROM wdv341_events");
$stmt->execute();
?>

<?php
    $table = "<table>";
    $table .= "<tr>";
		$table .= "<th><u>Event Name</u></th><th><u>Event Description</u></th><th><u>Event Presenter</u></th><th><u>Event Day</u></th><th><u>Event Time</u></th><th><u>Update</u></th><th><u>Delete</u></th>";
		$table .= "</tr>";

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
    {
			$eventId = $row["event_id"];
			$table .= "<tr>";
			$table .= "<td>". $row["event_name"] . "</td>";
			$table .= "<td>". $row["event_description"] . "</td>";
			$table .= "<td>". $row["event_presenter"] . "</td>";
			$table .= "<td>". $row["event_day"] . "</td>";
			$table .= "<td>". $row["event_time"] . "</td>";
      $table .= "<td><a href='updatingEvent.php?edit_id=" . $row['event_id'] . "'>Update</a></td>";
      $table .= "<td><a href='deletedEvent.php?del_id=" . $row['event_id'] . "'>Delete</a></td>";
			$table .= "</tr>";
		}

		  $table .= "</table>";

  ?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>WDV341 Intro PHP  - Display Events Example</title>
    <style>
		.eventBlock{
			width:500px;
			margin-left:auto;
			margin-right:auto;
      word-spacing: 5px;
      background-color:#CCC;

		}

    .displayEvent{
			text_align:left;
			font-size:18px;

		}

		.displayDescription {
			margin-left:100px;
    }


	</style>
</head>

<body>
    <h1>WDV341 Intro PHP</h1>
    <h2>Example Code - Display Events as formatted output blocks</h2>
    <h3>Events are available today.</h3>

<?php
	//Display each row as formatted output
	while($row =$stmt->fetch(PDO::FETCH_ASSOC))
	//Turn each row of the result into an associative array
  	{
      $event_name = $row['event_name'];
      $event_description = $row['event_description'];
      $event_presenter = $row['event_presenter'];
      $event_day = $row['event_day'];
      $event_time = $row['event_time'];
		//For each row you have in the array create a block of formatted text
?>
	<p>
        <div class="eventBlock">
            <div>
            	<span class="displayEvent"><?php echo $event_name; ?>Event:  </span>
            	<span class="displayDescription">Description: <?php echo $event_description; ?></span>
            </div>
            <div>
            	Presenter: <?php echo $event_presenter; ?>
            </div>
            <div>
            	<span class="displayTime">Time: <?php echo $event_time; ?></span>
            </div>
            <div>
            	<span class="displayDate">Day: <?php echo $event_day; ?></span>
            </div>
        </div>
    </p>

<?php
  	}//close while loop
//	$row->close();
//	$conn->close();	//Close the database connection
?>
</div>
</body>
</html>
