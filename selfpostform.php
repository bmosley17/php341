<?php

session_start(); //start session_start

//set up field database
$name = "";
$SSNum = "";

//set up error messages
$nameErrMsg = "";
$SSNumErrMsg = "";

$validForm = false;

if(isset($_POST["submit"]))
	{
		//setting up name value pairs
		$name = $_POST['inName'];
		$SSNum = $_POST['inEmail'];

		//setting up validation Functions

		function validateName($inName)
		{
			global $validForm, $nameErrMsg;

			$nameErrMsg = "";

			if($inName == "")
				{
					$validForm = false;
					$nameErrMsg = "Name cannont have spaces";
				}
				else{
						if(!preg_match('/^[a-zA-Z0-9\s.]*$/', $inName))
						{
							$validForm = false;
							$nameErrMsg = "Invalid Name";
						}
		} //end validateName()

		function validateSSNum($inNum)
			{
				global $validForm, $SSNumErrMsg;
				$SSNumErrMsg = "";

				if($inNum == "")
				 	{
						$validForm = false;
						$SSNumErrMsg = "Invalid SS#";
					}
						else{
							if(!preg_match('/^\d{3}-\d{2}-\d{4}$/', $inNum))
							{
								$validForm = false;
								$SSNumErrMsg = "Please enter like XXX-XX-XXXX";
						}
			}
	}//end validateSSNum()

$validForm = true; //switch for keeping track of any form validation errors

validateName($inName);
validateSSNum($inNum);

if($validForm)
	{
		$message = "All good";
	}
		else
		{
			$message = "Something went wrong";
		}
?>
<?php

	include 'dbConnect.php'; //connects to Database

	$wdv341_name = $_POST['inName'];
	$wdv341_ssnum = $_POST['inEmail'];
	$stmt->execute();

	$conn->close();


 ?>
<!DOCTYPE html>
<html lang= "en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WDV341 Intro PHP - Form Validation Example</title>
<style>

#orderArea	{
	width:600px;
	background-color:#CF9;
}

.error	{
	color:red;
	font-style:italic;
}
</style>
</head>

<body>
<h1>WDV341 Intro PHP</h1>
<h2>Form Validation Assignment

	<?php
				if($validForm)
				{
	        ?>
	            <h1><?php echo $message; ?></h1>

	        <?php
				}
				else	//display form
				{
	        ?>

</h2>
<div id="orderArea">
  <form id="form1" name="form1" method="post" action="selfpostform.php">
  <h3>Customer Registration Form</h3>
  <table width="587" border="0">
    <tr>
      <td width="117">Name:</td>
      <td width="246"><input type="text" name="inName" id="inName" size="40" value="<?php echo $name; ?> "/></td>
      <td width="210" class="error"><?php echo $nameErrMsg; ?></td>
    </tr>
    <tr>
      <td>Social Security</td>
      <td><input type="text" name="inEmail" id="inEmail" size="40" value="<?php echo $SSNum; ?>" /></td>
      <td class="error"><?php echo $SSNumErrMsg; ?></td>
    </tr>
    <tr>
      <td>Choose a Response</td>
      <td><p>
        <label>
          <input type="radio" name="RadioGroup1" id="RadioGroup1_0">
          Phone</label>
        <br>
        <label>
          <input type="radio" name="RadioGroup1" id="RadioGroup1_1">
          Email</label>
        <br>
        <label>
          <input type="radio" name="RadioGroup1" id="RadioGroup1_2">
          US Mail</label>
        <br>
      </p></td>
      <td class="error"></td>
    </tr>
  </table>
  <p>
    <input type="submit" name="submit" id="button" value="Register" />
    <input type="reset" name="button2" id="button2" value="Clear Form" />
  </p>
</form>
</div>
<?php

			}//end else
				?>
</body>
</html>
