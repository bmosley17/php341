<?php


	$eventName = "";
	$eventDesc= "";
	$eventPresenter = "";
	$eventDate = "";
	$eventTime = "";

  //error messages
	$eventNameErrMsg = "";
  $eventDescErrMsg = "";
	$eventPresenterErrMsg = "";
	$eventDateErrMsg = "";
	$eventTimeErrMsg = "";

	$validForm = False;

		if(isset($_POST["done"]))
	{
    $eventName = $_POST['eventName'];
  	$eventDesc= $_POST['eventDescription'];
  	$eventPresenter = $_POST['eventPresenter'];
  	$eventDate = $_POST['eventDate'];
  	$eventTime = $_POST['eventTime'];
		$Clicked = true;


		function validateName($inName){

			global $validForm, $eventNameErrMsg;

			if(preg_match('/^[a-zA-Z0-9,.!? ]*$/', $inName)){

			}
			else{

				$validForm = False;
				$eventNameErrMsg = "No numbers or special characters allowed";
			}
		}//end validateName()

		function validateDescription($inDescription){

			global $validForm, $eventDescErrMsg;

			if(preg_match('/^[a-zA-Z0-9,.!? ]*$/', $inDescription)){

			}
			else{

				$validForm = False;
				$eventDescErrMsg = "No numbers or special characters allowed";
			}
		}//end validateDescription()

		function validatePresenter($inPresenter){

			global $validForm, $eventPresenterErrMsg;

			if(preg_match('/^[a-zA-Z0-9,.!? ]*$/', $inPresenter)){

			}
			else{

				$validForm = False;
				$eventPresenterErrMsg = "No numbers or special characters allowed";
			}
		}//end validatePresenter()

		function validateDate($inDate){

			global $validForm, $eventDateErrMsg;



			if( strtotime(date("Y/m/d")) < strtotime($inDate) ){

			}
			else{

				$validForm = False;
				$eventDateErrMsg = "Please select a future date";
			}
		}//end validateDate()

		function validateTime($inTime){

			global $validForm, $eventTimeErrMsg;


			$time = strtotime($inTime);

			if(date('H',$time) < 22 && date('H',$time) > 6){

			}
			else{

				$validForm = False;
				$eventTimeErrMsg = "Please enter time between 6a and 10p";
			}
		}//end validateTime()

		$validForm = true;

		validateName($eventName);
		validateDescription($eventDesc);
		validatePresenter($eventPresenter);
		validateDate($eventDate);
		validateTime($eventTime);


		if($validForm == true){

			include("connectPDO.php");


			$stmt = $conn->prepare("UPDATE wdv341_event SET event_name = :event_name, event_description = :event_description, event_presenter = :event_presenter, event_date = :event_date, event_time = :event_time WHERE event_id = '$edit_id'");

      $edit_id = $_GET['event_id'];

			$stmt->bindParam(':event_name', $event_name);
			$stmt->bindParam(':event_description', $event_description);
			$stmt->bindParam(':event_presenter', $event_presenter);
			$stmt->bindParam(':event_date', $event_date);
			$stmt->bindParam(':event_time', $event_time);

			

      $stmt->execute();
      echo $inName. 'is entered';


      }
	}//end if done is clicked
?>



<!DOCTYPE html>
<html>
<head>
</head>
<body>
</h2>

	<?php

	include 'select.php';			//connects to the database

  $updatedName = $row['event_name'];
  $updatedDesc = $row['event_description'];
  $updatedPresenter = $row['event_presenter'];
  $updatedDate = $row['event_date'];
  $updatedTime = $row['event_time'];
?>


      <form id="form1" name="form1" method="post" action="updateEvent.php">
         <input type= "text" name= "eventName" id= "eventName" value= "<?php echo $updatedName?>" >
         <input type= "text" name= "eventDescription" id= "eventDescription" value= "<?php echo $updatedDesc?>">
         <input type= "text" name= "eventPresenter" id= "eventPresenter" value= "<?php echo $updatedPresenter; ?>">
         <input type= "text" name= "eventDate" id= "eventDate" value= "<?php echo $updatedDate?>">
         <input type= "text" name= "eventTime" id= "eventTime" value= "<?php echo $updatedTime?>">
         <input type= "submit" name= "done" value="insert">
      </form>
  </body>
</html>
