<!DOCTYPE html>
<html>
	<head>

	<link href= "../webpage.css" rel= "stylesheet" type= "text/css" />
  <link rel= "stylesheet" href= "form.css" type= "text/css" />
 	<style type= "text/css">

	.center {
		text-align: center;
	}

	body {
		background-color: grey;
		padding-bottom: 50px;
	}


	nav ul {
		font-weight: bold;
		list-style: none;
	}

	nav ul li a {
		padding: 5px;
		text-decoration: none;
		}

	nav ul li a:hover {
		background: #FF3399;
		border-radius: 10px;
		color: white;
	}

	header {
		padding: 5px;
	}

	h1, h2, h3, h4 {
		text-align: center;
		background-color: #FF3399;
		color: white;
	}

	del {
		color: pink;
	}


  #test_email {
    display: none;
  }

	</style>
		<title> Contact Page </title>

		<nav>
			<ul>
				<li><a href= "../index.html">Home </a></li>
				<li><a href= "../aboutme/index.html">About Me </a></li>
				<li><a href= "../webdev/index.html">Web Development </a></li>
				<li><a href= "../blog/2nov16.html">Blog </a></li>
				<li><a href= "index.html">The List </a></li>
				<li><a href= "contact.html">Contact Info </a></li>
			</ul>
		</nav>

	</head>
	<body>


    <div class="container">
  <form id="contact" action="formhandler.php" method="post">
    <h3>Quick Contact</h3>
    <h4>Let me know your thoughts/questions. Questions/concerns responded to within 48 hours!</h4>
    <fieldset>
      <input placeholder="Your name" name= "name" type="text" tabindex="1"  autofocus>
    </fieldset>
    <fieldset>
      <input placeholder="Your Email Address" name= "email" type="text" tabindex="2" >
    </fieldset>
    <fieldset>
      <input placeholder="Your Phone Number" name= "phone" type="text" tabindex="3" >
    </fieldset>
    <fieldset>
      <textarea placeholder="Type your Message Here...." name= "message" type= "text" tabindex="5" ></textarea>
    </fieldset>
    <fieldset>
      <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">Submit</button>
      <button name= "reset" type= "reset" id="contact-reset">Reset</button>
    </fieldset>
  </form>


</div>





	</body>
</html>
