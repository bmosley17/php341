<?php


$servername = "localhost";
$username = "root";
$password = "";
$database= "final";
$dvdName = "";
$releaseYear= "";
$dvdType = "";

//error messages
$dvdNameErrMsg = "";
$releaseYearErrMsg= "";
$dvdTypeErrMsg = "";

$validForm = False;


try {
   $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
   // set the PDO error mode to exception
   $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   

   if (isset($_POST['done']))
   {
     $dvdName = $_POST['dvd_name'];
   	$releaseYear = $_POST['release_year'];
   	$dvdType = $_POST['dvd_type'];

 		function validateDVDName($inName){

 			global $validForm, $dvdNameErrMsg;

 			$dvdNameErrMsg = "";

 			if($inName == "")
 				{
 					$validForm = false;
 					$dvdNameErrMsg = "Cannot be spaces";
 				}
 				else
 				{
 					if(!preg_match('/^[a-zA-Z0-9,.!? ]*$/', $inName))
 					{
 						$validForm = false;
 						$dvdNameErrMsg = "No numbers or special characters allowed";
 				}

 			}
 		}//end validateDVDName()

 		function validateReleaseYear($inYear)
 		{

 			global $validForm, $releaseYearErrMsg;
 			$releaseYearErrMsg = "";

 			if($inYear == "")
 				{
 					$validForm = false;
 					$releaseYearErrMsg = "4 digits only";
 				}
 					else{
 						if(!preg_match('/^(19|20)\d{2}$/', $inYear))
 						{
 							$validForm = false;
 							$releaseYearErrMsg = "Invalid Year. Must be from 1900 to present year";
 						}
 			}
 		}//end validateReleaseYear()

 		function validateDVDType($inType)
 		{
 			global $validForm, $dvdTypeErrMsg;

 			$dvdTypeErrMsg = "";

 			if($inType == "")
 				{
 					$validForm = false;
 					$dvdTypeErrMsg = "type can not contain spaces";
 				}
 					else{
 						if(!preg_match('/^[a-zA-Z0-9,.!? ]*$/', $inType))
 						{
 							$validForm = false;
 							$dvdTypeErrMsg = "Please enter BluRay or DVD";
 						}
 					}
 		}//end validateDVDType()


 		$validForm = true;

 		validateDVDName($dvdName);
 		validateReleaseYear($releaseYear);
 		validateDVDType($dvdType);


 		if($validForm == true){

   // prepare sql and bind parameters
   $stmt = $conn->prepare("INSERT INTO current_dvds (dvd_name, release_year, dvd_type)
   VALUES (:dvd_name, :release_year, :dvd_type)");
   $stmt->bindParam(':dvd_name', $dvd_name);
   $stmt->bindParam(':release_year', $release_year);
   $stmt->bindParam(':dvd_type', $dvd_type);

   $dvd_name= $_POST['dvd_name'];
   $release_year = $_POST['release_year'];
   $dvd_type = $_POST['dvd_type'];
   $stmt->execute();

   echo "New records created successfully";
   header("location:select.php");
 }

}//if done is clicked
 }//try ends

catch(PDOException $e)
   {
   echo "Connection failed: " . $e->getMessage();
   }


?>
<!DOCTYPE>
<html>
<head>
  <style>
    body{
      background-color: lightblue;
    }
    form {
      color: yellow;
      font-size: 2em;

    }
    form input{
      font-size: .7em
    }
  </style>
</head>
<body>
  <h1> New Movie? Please add here </h1>
<form method="post" action= "insert.php" >
  <label>Dvd Name</label>
 <input type= "text" name= "dvd_name" value= "<?php echo $dvdName; ?>"><span><?php echo $dvdNameErrMsg; ?></span><br/>
 <label>Release Year</label>
 <input type= "text" name= "release_year" value= "<?php echo $releaseYear; ?>"><span><?php echo $releaseYearErrMsg; ?><br/>
 <label>Dvd Type</label>
 <input type= "text" name= "dvd_type" value= "<?php echo $dvdType; ?>"><span><?php echo $dvdTypeErrMsg; ?><br/>
 <input type= "submit" name= "done" value="insert">
</form>
</body>
</html>
