<?php


$servername = "localhost";
$username = "root";
$password = "";
$database= "wdv341";


try {
   $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
   // set the PDO error mode to exception
   $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   echo "Connected successfully";

   // prepare sql and bind parameters
   $stmt = $conn->prepare("INSERT INTO wdv341_event (event_name, event_description, event_presenter, event_date, event_time)
   VALUES (:event_name,:event_description, :event_presenter, :event_date, :event_time )");
   $stmt->bindParam(':event_name', $event_name);
   $stmt->bindParam(':event_description', $event_description);
   $stmt->bindParam(':event_presenter', $event_presenter);
   $stmt->bindParam(':event_date', $event_date);
   $stmt->bindParam(':event_time', $event_time);


   echo "New records created successfully";
   }

catch(PDOException $e)
   {
   echo "Connection failed: " . $e->getMessage();
   }


?>



<form method="post">
  <input type= "text" name= "event_name" placeholder="event name">
  <input type= "text" name= "event_description" placeholder="event description">
  <input type= "text" name= "event_presenter" placeholder="event presenter">
  <input type= "text" name= "event_date" placeholder="event date">
  <input type= "text" name= "event_time" placeholder="event time">
  <input type= "submit" name= "done" placeholder="Insert Data">
</form>
