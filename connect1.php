 <?php


$servername = "localhost";
$username = "root";
$password = "";
$database= "bmosley17_wdv341";


try {
    $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully";

    // prepare sql and bind parameters
    $stmt = $conn->prepare("INSERT INTO wdv341_event (event_name, event_description, event_presenter, event_date, event_time)
    VALUES (:event_name,:event_description, :event_presenter, :event_date, :event_time )");
    $stmt->bindParam(':event_name', $event_name);
    $stmt->bindParam(':event_description', $event_description);
    $stmt->bindParam(':event_presenter', $event_presenter);
    $stmt->bindParam(':event_date', $event_date);
    $stmt->bindParam(':event_time', $event_time);

    // insert a row
    $event_name = "Biking Trip";
    $event_description = "Let's take a bike ride through Des Moines,IA";
    $event_presenter = "Brittani M";
    $event_date = "2018-04-01";
    $event_time = "09:00";
    $stmt->execute();

    //another row
    $event_name = "Spring Break starts";
    $event_description = "No school starting Monday the 12th";
    $event_presenter = "DMACC";
    $event_date = "2018-03-12";
    $event_time = "08:00";
    $stmt->execute();

    echo "New records created successfully";
    }

catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }


?>
