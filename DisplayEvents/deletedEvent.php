<?php


$servername = "localhost";
$username = "root";
$password = "";
$database= "wdv341";


try {
   $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
   $del_id = $_GET['del_id'];
   // set the PDO error mode to exception
   $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   echo "Connected successfully";

   // prepare sql and bind parameters
   $stmt = $conn->prepare("DELETE FROM wdv341_event where event_id = '$del_id' ");


   $stmt->execute();
   header("location:select.php");

 }


catch(PDOException $e)
   {
   echo "Connection failed: " . $e->getMessage();
   }


?>
