

<?php
$servername = "localhost";
$username = "root";
$password = "";
$database= "wdv341";

	try {
	   $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
	   // set the PDO error mode to exception
	   $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	   echo "Connected successfully";

		 $stmt = $conn->prepare("INSERT INTO wdv341_event (event_name, event_description, event_presenter, event_date, event_time)
		 VALUES (:event_name,:event_description, :event_presenter, :event_date, :event_time )");
		$stmt->bindParam(':event_name', $event_name);
		$stmt->bindParam(':event_description', $event_description);
		$stmt->bindParam(':event_presenter', $event_presenter);
		$stmt->bindParam(':event_date', $event_date);
		$stmt->bindParam(':event_time', $event_time);

		$event_name = "example name";
		$event_description = "example description";
		$event_presenter = "example presenter";
		$event_date = "2018-08-05";
		$event_time = "18:00";
		$stmt->execute();
	}
	catch(PDOException $e)
	    {
	    echo "Connection failed: " . $e->getMessage();
	    }
	?>
