<?php
//set up variables for every field:

$inName = "";
$inSSN = "";
$inResponse = "";
$HoneyPot = "";

$validForm = false;

$nameErrorMsg = "";
$ssnErrorMsg = "";
$responseErrorMsg = "";

function validateName(){
	global $inName, $nameErrorMsg, $validForm; //bring in variables
		$nameErrorMsg="";					   // clear the error message
	if($inName=="") {						  //if the name is empty, show the error msg
		$validForm = false;
		$nameErrorMsg = "Name field is required";
	}
}

function validateSSN(){
	global $inSSN, $ssnErrorMsg, $validForm;
	$nameErrorMsg="";
	$ssnFormat = "#^\d{9}$#";
	if($inSSN=="") //validates that the field isnt blank
		{
			$validForm=false;
			$ssnErrorMsg = "Social Security Number is required";
		}

	elseif(intVal($inSSN)==0)		 //validates that a number was entered
		{
			$validForm = false;
			$ssnErrorMsg= "Must enter a number";
		}

	elseif(!preg_match($ssnFormat, $inSSN)) //validates the number field
		{
			$validForm= false;
			$ssnErrorMsg = "Please enter a valid SSN: no spaces or dashes! ex:111223333";
		}
	}

	function validateResponse(){
		global $inResponse, $responseErrorMsg, $validForm;
		$responseErrorMsg = "";

		if($inResponse == "")
		{
			$validForm = false;
			$responseErrorMsg = "Please select a response method";
		}
	}


if(isset($_POST["submit"])) //if the form has been submitted do the following:
{

	//get the name/value pairs from POST and store them in variables.

	$inName = $_POST['inName'];
	$inSSN = $_POST['inSSN'];
	$inResponse = $_POST['inResponse'];
	$HoneyPot = $_POST['honeypot'];

	$validForm= true;

	validateName();
	validateSSN();
	validateResponse();
	if(!empty($HoneyPot))
		{
			echo "error, please try again";
		}

}



else //display the empty form
{

}

?>
<!DOCTYPE html>
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WDV341 Intro PHP - Form Validation Example</title>
<style>

#orderArea	{
	width:600px;
	background-color:#CF9;
}

.error	{
	color:red;
	font-style:italic;
	}

.honeypot {
			display: none;
		}
</style>
</head>

<body>
<h1>WDV341 Intro PHP</h1>
<h2>Form Validation Assignment

<?php

if ($validForm)			//If the form info is valid
{
?>
	<h3>Thank You!</h3>
    <p>Your information has been registered!</p>

<?php
}	//end the true branch of the form view area
else
{
?>
</h2>
<div id="orderArea">
  <form id="form1" name="form1" method="post" action="formValidation.php">
  <!--honeypot -->
  <input name="honeypot" type="text" id="honeypot" class="honeypot">

  <h3>Customer Registration Form</h3>
  <table width="587" border="0">
    <tr>
      <td width="117">Name:</td>
      <td width="246"><input type="text" name="inName" id="inName" size="40" value="<?php echo trim($inName); ?>"/></td>
      <td width="210" class="error"> <?php echo $nameErrorMsg; ?> </td>
    </tr>
    <tr>
      <td>Social Security</td>
      <td><input type="text" name="inSSN" id="inSSN" size="40" value="<?php echo $inSSN; ?>" /></td>
      <td class="error"> <?php echo $ssnErrorMsg; ?> </td>
    </tr>
    <tr>
      <td>Choose a Response</td>
      <td><p>
        <label>
          <input type="radio" name="inResponse" id="inResponse1" value="Phone" <?php if ($inResponse == 'Phone') { echo "checked"; } ?> />
          Phone</label>
        <br>
        <label>
          <input type="radio" name="inResponse" id="inResponse2" value="Email" <?php if ($inResponse == 'Email') { echo "checked"; } ?> />
          Email</label>
        <br>
        <label>
          <input type="radio" name="inResponse" id="inResponse3" value="US Mail" <?php if ($inResponse == 'US Mail') { echo "checked"; } ?> />
          US Mail</label>
        <br>
      </p></td>
      <td class="error"> <?php echo $responseErrorMsg; ?> </td>
	  <td class="error"> <?php echo $honeyErrorMsg; ?> </td>
    </tr>

  </table>
  <p>
    <input type="submit" name="submit" id="button" value="Register" />
    <input type="reset" name="button2" id="button2" value="Clear Form" />
  </p>
</form>
	<?php
	}	//end else branch for the View area
	?>
</div>

	<a href= "https://bitbucket.org/hmsieck/intro-php/src">View PHP code</a>

</body>
</html>
