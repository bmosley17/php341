<?php

 if (empty($_SESSION['username']))


	$dvdName = "";
	$releaseYear= "";
	$dvdType = "";

  //error messages
	$dvdNameErrMsg = "";
	$releaseYearErrMsg= "";
	$dvdTypeErrMsg = "";

	$validForm = False;

	$todaysDate = date("Y-m-d");


		if(isset($_POST["done"]))
	{
    $dvdName = $_POST['dvd_name'];
  	$releaseYear = $_POST['release_year'];
  	$dvdType = $_POST['dvd_type'];

		function validateDVDName($inName){

			global $validForm, $dvdNameErrMsg;

			$dvdNameErrMsg = "";

			if($inName == "")
				{
					$validForm = false;
					$dvdNameErrMsg = "Cannot be spaces";
				}
				else
				{
					if(!preg_match('/^[a-zA-Z0-9,.!? ]*$/', $inName))
					{
						$validForm = false;
						$dvdNameErrMsg = "No numbers or special characters allowed";
				}

			}
		}//end validateDVDName()

		function validateReleaseYear($inYear)
		{

			global $validForm, $releaseYearErrMsg;
			$releaseYearErrMsg = "";

			if($inYear == "")
				{
					$validForm = false;
					$releaseYearErrMsg = "4 digits only";
				}
					else{
						if(!preg_match('/^(19|20)\d{2}$/', $inYear))
						{
							$validForm = false;
							$releaseYearErrMsg = "Invalid Year";
						}
			}
		}//end validateReleaseYear()

		function validateDVDType($inType)
		{
			global $validForm, $dvdTypeErrMsg;

			$dvdTypeErrMsg = "";

			if($inType == "")
				{
					$validForm = false;
					$dvdTypeErrMsg = "type can not contain spaces";
				}
					else{
						if(!preg_match('/^[a-zA-Z0-9,.!? ]*$/', $inType))
						{
							$validForm = false;
							$dvdTypeErrMsg = "Please enter BluRay or DVD";
						}
					}
		}//end validateDVDType()


		$validForm = true;

		validateDVDName($dvdName);
		validateReleaseYear($releaseYear);
		validateDVDType($dvdType);


		if($validForm == true){

			$serverName = "localhost";
			$username = "root";
			$password = "";
			$database = "final";

			try {
			    $conn = new PDO("mysql:host=$serverName;dbname=$database", $username, $password);
			    // set the PDO error mode to exception
			    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			    echo "Connected successfully";
          $dvd_id = $_GET['dvd_id'];

					//PREPARE the SQL statement
						$stmt = $conn->prepare("UPDATE current_dvds SET dvd_name = :dvd_name, release_year = :release_year, dvd_type = :dvd_type WHERE dvd_id = '$dvd_id'");;

						//BIND the values to the input parameters of the prepared statement


						$stmt->bindParam(':dvd_name', $dvd_name);
						$stmt->bindParam(':release_year', $release_year);
						$stmt->bindParam(':dvd_type', $dvd_type);

						//EXECUTE the prepared statement
						$stmt->execute();

            $stmt->setFetchMode(PDO::FETCH_ASSOC);

		          $row=$stmt->fetch(PDO::FETCH_ASSOC);

			       $id=$row['event_id'];
      			$dvd_name=$row['dvd_name'];
      			$release_year=$row['release_year'];
      			$dvd_type=$row['dvd_type'];


						$message = "The Event has been Updated.";
}
						catch(PDOException $e)
						    {
						    echo "Connection failed: " . $e->getMessage();

						    }

      }
	}//end if done is clicked

?>



<!DOCTYPE html>
<html>
<head>
</head>
<body>
</h2>

	<?php

	include 'select.php';			//connects to the database

?>


<form method="post">
	<label>Dvd ID</label>
 <input type= "text" name= "dvd_id" value= ""><br/>
	<label>Dvd Name</label>
 <input type= "text" name= "dvd_name" value= "<?php echo $dvd_name?>"><br/>
 <label>Release Year</label>
 <input type= "text" name= "release_year"><br/>
 <label>Dvd Type</label>
 <input type= "text" name= "dvd_type"><br/>
 <input type= "submit" name= "done" value="insert">
</form>
  </body>
</html>
