<?php

		//variables
		$name = "";
		$SSNum = "";
		$response = "";

		//eror messages
		$nameErrMsg = "";
		$SSNumErrMsg = "";
		$responseErrMsg = "";

		$validForm = false;

		if(isset($_POST["submit"]))
		{
				$name = $_POST['inName'];
				$SSNum = $_POST['inEmail'];
				$response = isset($_POST['RadioGroup1']);

				function validateRadiogroups($inResponse){
					global $validForm, $responseErrMsg;
					$responseErrMsg = "";

					if(!$inResponse == "") {

          	$validForm = false;
			 			$responseErrMsg = "Please Select a Response Method";
        	}
				}//end validateRadiogroups()


				function validateSocialSecurity($inEmail) {
					global $validForm, $SSNumErrMsg;
					$SSNumErrMsg = "";

					if($inEmail == "")
						{
							$validForm = false;
							$SSNumErrMsg = "invalid SS#";
						}
							else {
								if(!preg_match('/^\d{3}-\d{2}-\d{4}$/', $inEmail))
								{
									$validForm = false;
									$SSNumErrMsg = "Please enter like XXX-XX-XXXX";
								}
						} //end validateSocialSecurity


				function validateName($inName)
				{
					global $validForm, $nameErrMsg;
					$nameErrMsg = "";

					if($inName == "")
						{
							$validForm = false;
							$nameErrMsg = "Invalid Name";
						}
						else{
								if(!preg_match('/^[a-zA-Z0-9\s.]*$/', $inName))
								{
									$validForm = false;
									$nameErrMsg = "Invalid Name";
								}
						} //end validateName

				}
			}

			$validForm = true;

			validateRadiogroups($inResponse);
			validateSocialSecurity($SSNum);
			validateName($name);


			if($validForm)
			{
				$message = "All good";
			}
				else
				{
						$message = "Something went wrong";
				}

		} //ends if submit



?>

<!DOCTYPE html>
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WDV341 Intro PHP - Form Validation Example</title>
<style>

#orderArea	{
	width:600px;
	background-color:#CF9;
}

.error	{
	color:red;
	font-style:italic;
}
</style>
</head>

<body>


<h1>WDV341 Intro PHP</h1>
<h2>Form Validation Assignment

	<?php
				if($validForm)
				{
	        ?>
	            <h1><?php echo $message; ?></h1>

	        <?php
				}
				else	//display form
				{
	        ?>
</h2>
<div id="orderArea">
  <form id="form1" name="form1" method="post" action="formValidationAssignment.php">
  <h3>Customer Registration Form</h3>
  <table width="587" border="0">
    <tr>
      <td width="117">Name:</td>
      <td width="246"><input type="text" name="inName" id="inName" size="40" value="<?php echo $name; ?>"/></td>
      <td width="210" class="error"><?php echo $nameErrMsg; ?></td>
    </tr>
    <tr>
      <td>Social Security</td>
      <td><input type="text" name="inEmail" id="inEmail" size="40" value="<?php echo $SSNum; ?>" /></td>
      <td class="error"><?php echo $SSNumErrMsg; ?></td>
    </tr>
    <tr>
      <td>Choose a Response</td>
      <td><p>
        <label>
          <input type="radio" name="RadioGroup1" id="RadioGroup1_0" value= "phone" <?php if ($response === 'phone') { echo "checked"; } ?>>
          Phone</label>
        <br>
        <label>
          <input type="radio" name="RadioGroup1" id="RadioGroup1_1" value= "email" <?php if ($response === 'email') { echo "checked"; } ?>>
          Email</label>
        <br>
        <label>
          <input type="radio" name="RadioGroup1" id="RadioGroup1_2" value= "mail" <?php if ($response === 'mail') { echo "checked"; } ?>>
          US Mail</label>
        <br>
      </p></td>
      <td class="error"><?php echo $responseErrMsg ?></td>
    </tr>
  </table>
  <p>
    <input type="submit" name="submit" id="button" value="Register" />
    <input type="reset" name="button2" id="button2" value="Clear Form" />
  </p>
</form>
</div>
<?php

			}//end else
				?>
</body>
</html>
